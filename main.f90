PROGRAM MAIN

  !******Appel des modules******



  USE MODULE_NEWTON_ITERATION_EPAISSEUR           ! Acces au module pour calculer H
  USE MODULE_NEWTON_ITERATION_CHALEUR
  USE STRESS_FIELD
  USE CONSTANTE            !Acces au module qui contient les constantes
  USE IS_NAN_OR_INFTY
  USE MODULE_OUTPUT
  USE MODULE_INITIALISATION
  USE MODULE_SURFACE_TEMPERATURE


  !****INITIALISATION DES VARIABLE*******


  IMPLICIT NONE         


  DOUBLE PRECISION                                            :: epsilon,Dt,Dr,epsilon2
  INTEGER                                             :: i,ndyke,z,Ite_Glob,sample,Init
  INTEGER                                             :: err1,err2,j,k,k1,k2,test
  DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE       :: H,P,T,Xi,BL,Ts
  DOUBLE PRECISION, DIMENSION(:)  , ALLOCATABLE       :: Srr,Stt
  INTEGER                                             :: M,Mtot,Tmax,Cas,compteur
  DOUBLE PRECISION                                    :: tmps,Te,N3,N3t,N4,N4t,hmaxx,N5,N5t
  DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE         :: dist,ray
  DOUBLE PRECISION                                    :: R,qa
  DOUBLE PRECISION, PARAMETER                         :: pi=3.14159265
  DOUBLE PRECISION                                        :: sigma,el,grav,nu,delta0,F1,F2,F3,F4,Ta

  CHARACTER(LEN=300)                                  :: Output_racine,Input_Racine,Input_Data_Name
  CHARACTER(LEN=300)                                  :: Format_NSD,Format_O
  CHARACTER(LEN=300)                                  :: Format_NSD_Init_0,Format_NSD_Init_1
  CHARACTER(LEN=300)                                  :: Format_Input_Data,Format_RV,Format_Backup
  
  CALL CONST(Mtot,Te,Dt,Dr,el,grav,sigma,nu,sample,delta0,F1,F2,F3,F4,Ta&
       &,epsilon,epsilon2,Format_O,Format_NSD,Init,Input_Racine,Output_Racine,Input_Data_Name&
       &,Format_NSD_Init_0,Format_NSD_Init_1,Format_Input_Data,Format_RV,Format_Backup)

  ALLOCATE(H(1:Mtot,5),P(1:Mtot,3),Xi(1:Mtot,6),T(1:Mtot,4),Ts(1:Mtot,5),BL(1:Mtot,3)&
  &,Srr(1:Mtot),Stt(1:Mtot),ray(1:Mtot),dist(1:Mtot),stat=err1)
  IF (err1>1) STOP       

 CALL  INITIALISATION(Format_O,Format_NSD,Mtot,H,T,Ts,P,Xi,BL,dist,ray,k,k1,k2,z,tmps,Te,&
       &Dt,Dr,epsilon,epsilon2,el,grav,sigma,nu,F1,F2,F3,F4,Ta,sample,delta0,Init,compteur,&
       &Input_Data_Name,Input_racine,Output_Racine&
       &,Format_NSD_Init_0,Format_NSD_Init_1,Format_Input_Data,Format_RV,Format_Backup)

  print*,el,grav,nu

  TEMPS: DO WHILE (tmps<Te)
     
     OPEN(unit=3,file='Output.txt')
     
!!! ECRITURE BACKUP
     Cas=0
     CALL  OUTPUT(Format_O,Dt,Mtot,H,T,P,Xi,BL,Ts,dist,ray,k,k1,k2,z,compteur,tmps,&
       &Output_Racine,Srr,Stt,delta0,Cas,sample,Format_RV,Format_Backup)

     H(:,1) = H(:,3); H(:,5) = H(:,3) 
     Xi(:,1) = Xi(:,3); Xi(:,5) = Xi(:,3)
     T(:,1) = T(:,3); BL(:,1) = BL(:,3)
     T(:,4) = 0; 
     Ts(:,1) = Ts(:,3); Ts(:,5) = Ts(:,3)

     Ite_Glob = 0
     N3 = 20.d0; N3t = N3; N4 = 20.d0; N4t = N4
     N5 = 20.d0; N5t = N5
     ITERATION_GLOBALE: DO

        H(:,2) = H(:,3); Xi(:,2) = Xi(:,3); T(:,2) = T(:,3); BL(:,2) = BL(:,3);  Ts(:,2) = Ts(:,3)

        CALL  NEWTON_ITERATION_EPAISSEUR(H,P,T,BL,Ts,Dt,Dr,Mtot,dist,ray,el,grav,sigma,nu,delta0,epsilon)
        !CALL  NEWTON_ITERATION_CHALEUR(H,P,T,Xi,BL,Ts,Dt,Dr,dist,ray,Mtot,sigma,nu,delta0,el,grav,epsilon,k)
       ! CALL  QUARTIC_SURFACE_TEMP(H,BL,T,Ts,F1,F2,F3,F4,Ta,Mtot,k,Dr,Sigma)

	DO i = 1,Mtot,1
           IF (Xi(i,5)>1D-10) THEN
              Xi(i,6) = (Xi(i,3)-Xi(i,5))/Xi(i,5)
           ELSE
              Xi(i,6) = Xi(i,5)
           END IF
        END DO

        N3 = ABS(MAXVAL((H(:,3)-H(:,5))/(H(:,5)))); N4 = ABS(MAXVAL((Xi(:,6))))
        N5 = ABS(MAXVAL((Ts(:,3)-Ts(:,5))/(Ts(:,5))))


       
        WRITE(3,*),'Iteration_Global',Ite_Glob,N3,N4
        IF (Ite_Glob>20000) THEN
           PRINT*,'Erreur Ite Global'
           IF (k>10)STOP
        END IF
        IF (N3<epsilon2 .AND. N4<epsilon2) EXIT
        N3t = N3; N4t = N4; N5t = N5
        H(:,5) = H(:,3); Xi(:,5) = Xi(:,3); Ts(:,5)=Ts(:,3)
        Ite_Glob = Ite_Glob+1

     END DO ITERATION_GLOBALE

!!! CALCULE DU CHAMP DE CONTRAINTE

     CALL STRESS_ELASTIC_FIELD(Srr,Stt,H,dist,Dr,Mtot)

!!! ECRITURE DU FICHIER DONNEE ET DU BACKUP

     Cas = 1
     CALL  OUTPUT(Format_O,Dt,Mtot,H,T,P,Xi,BL,Ts,dist,ray,k,k1,k2,z,compteur,tmps,&
          &Output_Racine,Srr,Stt,delta0,Cas,sample,Format_RV,Format_Backup)

!!! FIN DE LA BOUCLE
     k = k+1
    print*,'Iteration',k,'t',tmps,'h',H(1,3),'R',COUNT(H(:,3)>0)*Dr
     WRITE(3,*),k,tmps,H(1,3),T(1,3),BL(10,3),Ts(1,3)
     ClOSE(3)
     tmps = tmps+Dt
  END DO TEMPS


  DEALLOCATE(H,T,P,Xi,BL,Ts)
  DEALLOCATE(dist,ray)


END PROGRAM MAIN




