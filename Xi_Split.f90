MODULE XI_SPLIT

CONTAINS

  SUBROUTINE Xi_Splitting(Xi,H,BL,T,Ts,N,Dt)

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:,:)  , INTENT(IN)             :: H
    DOUBLE PRECISION  ,DIMENSION(:,:)  , INTENT(INOUT)       :: T,BL,Xi,Ts
    DOUBLE PRECISION                           , INTENT(IN)             :: Dt
    INTEGER                                          ,INTENT(IN)              :: N

    INTEGER                                                     :: i

  

!!! RECONSTRUCTION DU VECTEURS COUCHE LIMITES

    DO i=1,N
       IF ( Xi(i,3)>0.d0 .AND. Xi(i,3)<H(i,3)/3.d0*(2.d0+Ts(i,3))) THEN
          BL(i,3) = H(i,3)
          T(i,3) = 3.d0/2.d0*(Xi(i,3)/H(i,3)-Ts(i,3)/3.d0)
       ELSEIF (Xi(i,3)>H(i,3)/3.d0*(2.d0+Ts(i,3)) .AND. Xi(i,3)<H(i,3)) THEN
          BL(i,3) = 3*(Xi(i,3)-H(i,3))/(Ts(i,3)-1)
          T(i,3) = 1.d0
       ELSE
          PRINT*,'ERREUR DANS XI_SPLIT: Xi(',i,') = ',Xi(i,3),H(i,3)
          !STOP

       ENDIF
    END DO


  END SUBROUTINE Xi_Splitting

END MODULE Xi_Split
