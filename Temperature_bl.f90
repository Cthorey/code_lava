MODULE TEMPERATURE_MODULE

CONTAINS

  SUBROUTINE  TEMPERATURE(Te,col,N,Xi,H,T,BL,Ts,P,dist,ray,Dr,nu,qa,delta0,el,grav)


    !### INTIALISATION DES VARIABLES ###!

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)          :: Te
    INTEGER           ,INTENT(IN)                             :: col,N
    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(IN)             :: Xi,H,T,BL,Ts,P
    DOUBLE PRECISION  ,INTENT(IN)                             :: Dr 
    DOUBLE PRECISION  ,DIMENSION(:), INTENT(IN)               :: qa,dist,ray
    DOUBLE PRECISION  ,INTENT(IN)                             :: nu,delta0,el,grav

    DOUBLE PRECISION                                          :: Ai,Bi
    DOUBLE PRECISION                                          :: h_a,h_a2,h_a3,delta_a,delta_a2,eta_a,zeta_a,Omega_a,T_a
    DOUBLE PRECISION                                          :: Psi_a,Sigma_a,Ts_a
    DOUBLE PRECISION                                          :: h_b,h_b2,h_b3,delta_b,delta_b2,eta_b,zeta_b,Omega_b,T_b
    DOUBLE PRECISION                                          :: DeltaT_a,DeltaT_b
    DOUBLE PRECISION                                          :: Psi_b,Sigma_b,Ts_b
    DOUBLE PRECISION                                          :: loss
    INTEGER                                                   :: i


    !### REMPLISSAGE DE T ###!

    DO i=1,N,1
 
       IF1: IF (i/=1 .AND. i/=N) THEN
          Ai=(ray(i)/(dist(i)*Dr))
          Bi=(ray(i-1)/(dist(i)*Dr))
          h_a=0.5d0*(H(i+1,3)+H(i,3));h_b=0.5d0*(H(i,3)+H(i-1,3))
          h_a2=0.5d0*(H(i+1,3)**2+H(i,3)**2);h_b2=0.5d0*(H(i,3)**2+H(i-1,3)**2)
          h_a3=0.5d0*(H(i+1,3)**3+H(i,3)**3);h_b3=0.5d0*(H(i,3)**3+H(i-1,3)**3)
          delta_a=0.5d0*(BL(i+1,col)+BL(i,col));delta_b=0.5d0*(BL(i,col)+BL(i-1,col))
          delta_a2=0.5d0*(BL(i+1,col)**2+BL(i,col)**2);delta_b2=0.5d0*(BL(i,col)**2+BL(i-1,col)**2)
          eta_a = grav*(H(i+1,3)-H(i,3))/Dr
          eta_b = grav*(H(i,3)-H(i-1,3))/Dr
          T_a=0.5d0*(T(i+1,col)+T(i,col));T_b=0.5d0*(T(i,col)+T(i-1,col))
          Ts_a=0.5d0*(Ts(i+1,col)+Ts(i,col));Ts_b=0.5d0*(Ts(i,col)+Ts(i-1,col))     

          DeltaT_a = T_a-Ts_a; DeltaT_b = T_b-Ts_b                    
          Omega_a = (nu*h_a2+(1-nu)*(T_a*h_a2-(5.d0/28.d0)*DeltaT_a*delta_a**2))*eta_a
          Omega_b = (nu*h_b2+(1-nu)*(T_b*h_b2-(5.d0/28.d0)*DeltaT_b*delta_b**2))*eta_b
          Sigma_a = (nu*delta_a*DeltaT_a*(delta_a2/20.d0-h_a2/6.d0)+&
               & (1-nu)*T_a*DeltaT_a*((-1.d0/6.d0)*delta_a*h_a2+delta_a2*(5.d0*h_a/28.d0-delta_a/20.d0)))*eta_a
          Sigma_b = (nu*delta_b*DeltaT_b*(delta_b2/20.d0-h_b2/6.d0)+&
               & (1-nu)*T_b*DeltaT_b*((-1.d0/6.d0)*delta_b*h_b2+delta_b2*(5.d0*h_b/28.d0-delta_b/20.d0)))*eta_b

          
       ELSE IF (i==N) THEN
          Bi=(ray(i-1)/(dist(i)*Dr))
          h_b=0.5d0*(H(i,3)+H(i-1,3))
          h_b2=0.5d0*(H(i,3)**2+H(i-1,3)**2)
          h_b3=0.5d0*(H(i,3)**3+H(i-1,3)**3)
          delta_b=0.5d0*(BL(i,col)+BL(i-1,col))
          delta_b2=0.5d0*(BL(i,col)**2+BL(i-1,col)**2)
          eta_b=(grav*(H(i,3)-H(i-1,3))+el*(P(i,3)-P(i-1,3)))/Dr
          T_b=0.5d0*(T(i,col)+T(i-1,col))
          Ts_b = 0.5d0*(Ts(i,col)+Ts(i-1,col))
          DeltaT_b = T_b-Ts_b                    
          
          Omega_b = (nu*h_b2+(1-nu)*(T_b*h_b2-(5.d0/28.d0)*DeltaT_b*delta_b**2))*eta_b
          Sigma_b = (nu*delta_b*DeltaT_b*(delta_b2/20.d0-h_b2/6.d0)+&
               & (1-nu)*T_b*DeltaT_b*((-1.d0/6.d0)*delta_b*h_b2+delta_b2*(5.d0*h_b/28.d0-delta_b/20.d0)))*eta_b

       ELSE IF (i==1) THEN
          Ai=(ray(i)/(dist(i)*Dr))
          h_a=0.5d0*(H(i+1,3)+H(i,3))
          h_a2=0.5d0*(H(i+1,3)**2+H(i,3)**2)
          h_a3=0.5d0*(H(i+1,3)**3+H(i,3)**3)
          delta_a=0.5d0*(BL(i+1,col)+BL(i,col))
          delta_a2=0.5d0*(BL(i+1,col)**2+BL(i,col)**2)
          eta_a=(grav*(H(i+1,3)-H(i,3))+el*(P(i+1,3)-P(i,3)))/Dr
          T_a=0.5d0*(T(i+1,col)+T(i,col))
          Ts_a=0.5d0*(Ts(i+1,col)+Ts(i,col))             
          DeltaT_a = T_a-Ts_a;
          Omega_a = (nu*h_a2+(1-nu)*(T_a*h_a2-(5.d0/28.d0)*DeltaT_a*delta_a**2))*eta_a
          Sigma_a = (nu*delta_a*DeltaT_a*(delta_a2/20.d0-h_a2/6.d0)+&
               & (1-nu)*T_a*DeltaT_a*((-1.d0/6.d0)*delta_a*h_a2+delta_a2*(5.d0*h_a/28.d0-delta_a/20.d0)))*eta_a




       END IF IF1
       
       IF2: IF (BL(i,col)==0.d0) THEN
          loss=0.d0
       ELSE
          loss=-(2*(T(i,col)-Ts(i,col))/BL(i,col))
          !print*,i,loss,T(i,col),Ts(i,col)
       END IF IF2

       IF3: IF (i==1) THEN
          Te(i)=qa(i)+loss+Ai*Omega_a*Xi(i,col)+Ai*Sigma_a
       ELSEIF (i==N) THEN
          Te(i)=qa(i)+loss-Bi*Omega_b*Xi(i-1,col)-Bi*Sigma_b
        ELSE
           Te(i)=Ai*Omega_a*Xi(i,col)-Bi*Omega_b*Xi(i-1,col)&
               &+Ai*Sigma_a-Bi*Sigma_b &
               &+loss+qa(i)

        END IF IF3

    END DO

  END SUBROUTINE TEMPERATURE

END MODULE TEMPERATURE_MODULE

