PROGRAM CLEMENT

  USE Solve_Real_Poly

  IMPLICIT NONE
  COMPLEX(8) :: root1,root2,root3,root4
  INTEGER :: code
  DOUBLE PRECISION, DIMENSION(5):: a

!    =0 degenerate equation
!    =1 one real root
!    =21 two identical real roots
!    =22 two distinct real roots
!    =23 two complex roots
!    =31 multiple real roots
!    =32 one real and two complex roots
!    =33 three distinct real roots
!    =41
!    =42 two real and two complex roots
!    =43
!    =44 four complex roots


  a(1)=3.d0
  a(2)=6.d0
  a(3)=-123.d0
  a(4)=-126.D0
  a(5)=1080.d0

  CALL rpoly(a(1),a(2),a(3),a(4),a(5),code,root1,root2,root3,root4)
  PRINT*,code,root1,root2,root3,root4
END PROGRAM CLEMENT


