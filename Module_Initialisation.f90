MODULE MODULE_INITIALISATION

  USE CONSTANTE

CONTAINS

  SUBROUTINE  INITIALISATION(Format_O,Format_NSD,Mtot,H,T,Ts,P,Xi,BL,dist,ray,k,k1,k2,z,tmps,Te,&
       &Dt,Dr,epsilon,epsilon2,el,grav,sigma,nu,F1,F2,F3,F4,Ta,sample,delta0,Init,compteur,&
       &Input_Data_Name,Input_racine,Output_Racine&
       &,Format_NSD_Init_0,Format_NSD_Init_1,Format_Input_Data,Format_RV,Format_Backup)
   
    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(INOUT)    :: H,T,P,Xi,BL,Ts
    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)    :: dist,ray
    DOUBLE PRECISION                        , INTENT(INOUT)     :: tmps
    INTEGER                                        , INTENT(INOUT)     :: k,k1,k2,z,Mtot,compteur,Init
    DOUBLE PRECISION                        , INTENT(INOUT)      :: Te
    INTEGER                                       , INTENT(INOUT)      :: sample
    DOUBLE PRECISION                        , INTENT(INOUT)      :: sigma,el,grav,nu,delta0,F1,F2,F3,F4,Ta! Nombre sans dimension du problème
    DOUBLE PRECISION                        , INTENT(INOUT)      :: Dt,Dr,epsilon,epsilon2
    CHARACTER(LEN=300)                  , INTENT(INOUT)      :: Output_Racine,Format_O,Format_NSD
    CHARACTER(LEN=300)                  , INTENT(INOUT)      :: Input_Racine,Input_Data_Name
    CHARACTER(LEN=300)                  , INTENT(INOUT)      :: Format_NSD_Init_0,Format_NSD_Init_1
    CHARACTER(LEN=300)                  , INTENT(INOUT)      :: Format_Input_Data,Format_RV,Format_Backup

    CHARACTER(LEN=300)                                             :: Input_Name_NSD,Input_Data
    LOGICAL                                                        :: FILE_EXISTS
    INTEGER                                                        :: i

    
    SELECT CASE (Init)

    CASE(0)
       CALL CONST(Mtot,Te,Dt,Dr,el,grav,sigma,nu,sample,delta0,F1,F2,F3,F4,Ta,&
       &epsilon,epsilon2,Format_O,Format_NSD,Init,Input_Racine,Output_Racine,Input_Data_Name&
       &,Format_NSD_Init_0,Format_NSD_Init_1,Format_Input_Data,Format_RV,Format_Backup)
       compteur=0 
       dist=0;ray=0
       DO i=1,Mtot,1
          dist(i)=(i-0.5d0)*Dr
          ray(i)=dist(i)+0.5d0*Dr
       END DO
       H(:,:)=0.d0;P(:,:)=0.d0;T(:,:)=0.d0;Ts(:,:)=0d0;BL(:,:)=0.d0
       !H(1:10,:)=0.001;T(1:10,:)=1d0;Ts(1:10,:)=1d0;BL(1:10,:)=H(1:10,:)
       Xi(:,1:3)=H(:,1:3)*T(:,1:3)-(1.d0/3.d0)*(T(:,1:3)-Ts(:,1:3))*BL(:,1:3)
       k=0;k1=1;k2=1;z=0;tmps=0

    CASE(1)
       WRITE(Input_Name_NSD,Format_NSD_Init_1)Input_Racine,'NbSsDim.txt'
       print*,Input_Name_NSD
       INQUIRE(FILE=Input_Name_NSD, EXIST=FILE_EXISTS)
       IF (FILE_EXISTS) THEN
          OPEN(1,file=Input_Name_NSD)
          READ(1,Format_NSD),sigma,el,grav,delta0,nu,F1,F2,F3,F4,Ta,Dt,Dr,epsilon,epsilon2,Mtot,sample
          CLOSE(1)
       ELSEIF( .NOT. FILE_EXISTS) THEN
          PRINT*,'ERREUR: PAS DE FICHIER AVEC LES NOMBRES SANS DIMENSIONS. RECOMMENCER LA SIMU DEPUIS LE DEPART'
          STOP
        ENDIF

       WRITE(Input_Data,Format_Input_Data)Input_Racine,Input_Data_Name
       print*,Input_Data
       INQUIRE(FILE=Input_Data, EXIST=FILE_EXISTS)  
       IF (FILE_EXISTS) THEN
          OPEN(1,file=Input_Data)
          DO i=1,Mtot,1
             READ(1,Format_O)k,k1,k2,z,compteur,Mtot,tmps,dist(i),ray(i)&
                  &,H(i,1),H(i,2),H(i,3),Xi(i,1),Xi(i,2),Xi(i,3),P(i,1),P(i,2),P(i,3),T(i,1),T(i,2),T(i,3)&
                  &,BL(i,1),BL(i,2),BL(i,3),Ts(i,1),Ts(i,2),Ts(i,3)
          END DO
          CLOSE(1)
          k1 = k1+1
          k2 = k2+1

       ELSEIF( .NOT. FILE_EXISTS) THEN
          PRINT*,'ERREUR: PAS DE FICHIER INPUT. CHOISIR LE MODE INIT=0 AND TRY AGAIN'
          STOP
       ENDIF
       
    END SELECT

     END SUBROUTINE INITIALISATION
     
   END MODULE MODULE_INITIALISATION
      
