MODULE MODULE_SURFACE_TEMPERATURE

!http://jblevins.org/mirror/amiller/rpoly.f90
  USE Solve_Real_Poly

CONTAINS

  SUBROUTINE   QUARTIC_SURFACE_TEMP(H,BL,T,Ts,N1,N2,N3,N4,Ta,Mtot,k,Dr,Sigma)

    IMPLICIT NONE

    DOUBLE PRECISION  , DIMENSION(:,:), INTENT(INOUT)  :: BL,T,Ts,H
    DOUBLE PRECISION                          , INTENT(IN)        :: N1,N2,N3,N4,TA,Dr,sigma
    INTEGER                                         , INTENT(IN)         :: Mtot,k

    LOGICAL                                                                    :: CHO
    INTEGER                                                                    :: Err1       
    DOUBLE PRECISION                                                    :: a(5),zr(5),zi(5)
    DOUBLE PRECISION                                                     :: sol       
    LOGICAL                                                                    :: fail
    INTEGER                                                                    :: i,j,ndyke,N,Tmax,degree


    ndyke=sigma/Dr
    CHO=COUNT(H(:,1)>0.d0)<ndyke
    SELECT CASE (CHO)
    CASE(.TRUE.)
       Tmax=ndyke    ! Cas ou on donne pas de profile initiale...
    CASE(.FALSE.)
       Tmax=COUNT(H(:,1)>0.d0)
    END SELECT
    
    N=Tmax
    degree = 4

    
    boucle: DO i=1,N,1
       
       a(1) = N1*N2**4*BL(i,3)
       a(2) = 4*N1*N2**3*BL(i,3)
       a(3) = 6*N1*N2**2*BL(i,3)
       a(4) = 4*N1*N2*BL(i,3)+N4*BL(i,3)+2.d0
       a(5) = BL(i,3)*(N1-N3-N4*Ta-2*T(i,3))
       
       CALL rpoly(a,degree,zr,zi,fail)

       sol = 0
       DO j = 1,4,1
          IF (zr(j)<1.d0 .AND. zr(j)>0.d0 .AND. zi(j)<1D-12) THEN
             sol = zr(j)
          ENDIF
       END DO
      
       IF (sol == 0) THEN
          PRINT*,zr
          PRINT*,zi
          PRINT*,BL(i,3),BL(i,1),T(i,3),T(i,1)
          PRINT*,i,'Pas trouver de sol pour le quartic'
          STOP
       ENDIF
    
       Ts(i,3) = T(i,3)
       Ts(i,2) = T(i,3)

    ENDDO boucle

   ! print*,'Module_Surface',Ts(1,3),Ts(2,3)

  END SUBROUTINE QUARTIC_SURFACE_TEMP

END MODULE MODULE_SURFACE_TEMPERATURE
